import express from 'express';
import config from 'config';
import fetch from 'node-fetch';
import * as socketio from 'socket.io';
import { configure, getLogger } from 'log4js';
import CecController = require('cec-controller');

const logger = getLogger();
configure({
    appenders: { /*file: { type: 'file', filename: 'app.log' }, */ out: { type: 'stdout' } },
    categories: { default: { appenders: ['out'], level: 'debug' } }
});

// Create a new express application instance
const app: express.Application = express();
const port : number = config.get('port');
const wsPort = port + 1;
const publicDir = __dirname.replace('/dist','') + '/public';
const openweathermapUrl = 'https://api.openweathermap.org/data/2.5/forecast';
const weekdays = ['Sonntag','Montag','Dienstag','Mittwoch','Donnerstag','Freitag','Samstag'];

app.get('/weather_img', (req: express.Request, res: express.Response) => {
  fetch(`${openweathermapUrl}?q=${config.get('openweathermap.city')}&appid=${config.get('openweathermap.key')}&cnt=1`).then(x => x.json()).then(x => {
    res.send(x.list.map((x:any) => `http://openweathermap.org/img/wn/${x.weather[0].icon}@2x.png`).join(''));
  });
});
app.get('/weather', (req: express.Request, res: express.Response) => {
  fetch(`${openweathermapUrl}?q=${config.get('openweathermap.city')}&appid=${config.get('openweathermap.key')}&cnt=5&units=metric`).then(x => x.json()).then(x => {
    res.json(x.list.map((x:any) => ({dt: x.dt, min: x.main.temp_min, max: x.main.temp_max, icon: `http://openweathermap.org/img/wn/${x.weather[0].icon}@2x.png`})));
  });
});
app.get('/hue', (req: express.Request, res: express.Response) => {
    let data : {ip:string, username: string, lights: string} = config.get("hue");
    let regex = new RegExp(data.lights);
    fetch(`http://${data.ip}/api/${data.username}/lights`).then(x => x.json()).then(hueresp => {
      let lights = Object.values(hueresp).map((x,i) => {
        x.id = parseInt(Object.keys(hueresp)[i]);
        return x;
      });
      let ids = lights.filter((light:any) => light.name.match(regex)).map((light:any) => light.id);
      res.json({ip: data.ip, username: data.username, lights: ids});
    })
});
app.get('/time', (req: express.Request, res: express.Response) => {
  res.send(new Date().getTime()+'');
});
app.get('/fitone', (req: express.Request, res: express.Response) => {
  fetch(`https://www.fit-one.de/${config.get('fitone.location')}/kursplan.html`).then(x => x.text()).then(html => {
    let weekday = weekdays[new Date().getDay()];

    //cutout relative html part
    const coursesNameStartString = 'class="courses">'
    const weekdayStartString = 'class="day">'
    html = html.substring(html.indexOf(weekdayStartString + weekday));
    html = html.substring(html.indexOf(coursesNameStartString) + coursesNameStartString.length);
    html = html.substring(0, html.indexOf(coursesNameStartString));
    
    //iterate through events
    const courseNameStartString = 'class="courseName">'
    const courseNameEndString = '</span>'
    const courseTimeStartString = 'class="courseTime">'
    let index = 0;
    let courses = [];
    while((index = html.indexOf(courseNameStartString)) != -1){
      index += courseNameStartString.length;
      let name = html.substring(index, html.indexOf(courseNameEndString, index));
      index = html.indexOf(courseTimeStartString, index) + courseTimeStartString.length;
      let time = html.substring(index, index + 5);
      courses.push({name: name, time: time});
      html = html.substring(index);
    }
    res.send(courses);
  }).catch(() => {
    res.json({});
  });
});

// HDMI CEC
var cecCtl = new CecController();
cecCtl.on('ready', (controller: any) => {
  //console.log(controller);
  //controller.dev0.turnOn();

  cecCtl.on('keypress', (keyName: string) => {
    io.emit('keypress', keyName);
  });
});
cecCtl.on('error', () => logger.info('CEC-Client is not working!'));

// setup socket.io connection
let http = require("http").Server(app);
let io = require("socket.io")(http);
io.on("connection", function(socket: socketio.Socket) {
  socket.on("message", function(socket: socketio.Packet) {
  });
  socket.on("debug", (msg: socketio.Packet) => {
    logger.debug(`[CLIENT] ${msg}`)
  });
});
http.listen(wsPort, function(){
  logger.info(`WS listening on port ${wsPort}!`);
});


app.use(express.static(publicDir));

app.listen(port, function () {
  logger.info(`HTTP listening on port ${port} and directory ${publicDir}!`);
});

process.on('uncaughtException', (error: any) => {
  logger.error(`Uncaught Exception: ${error}`);
});
process.on('unhandledRejection', (reason: any, p: any) => {
  logger.error(`Unhandled Rejection: ${reason} ${p}`);
});