document.addEventListener('DOMContentLoaded', () => {

    // helper
    let lz = (x) => ((x>9?'':'0')+x);
    let start = (cb, interval) => { setInterval(() => {cb();}, interval); cb();}
    let toDate = (str) => {
        let d = new Date(str*1000);
        return `${lz(d.getHours())}:${lz(d.getMinutes())}`;
    };
    let show = (str) => {document.body.className = str;}
    let sound = () => {new Audio('http://localhost:3000/media/eventually.mp3').play()};

    // time
    let timeElement = document.querySelector("#time");
    let updateTime = () => {
        let t = new Date();
        timeElement.innerHTML = `${lz(t.getHours())}:${lz(t.getMinutes())}:${lz(t.getSeconds())}`;
    };
    start(() => { updateTime()}, 1000);

    // weather
    let weatherImgElement = document.querySelector("#weather_img");
    let weatherElement = document.querySelector("#weather table");
    let weatherUpdate = () => {
        fetch('/weather_img').then(x => x.text()).then(x => {
            weatherImgElement.src = x;
        });
        fetch('/weather').then(x => x.json()).then(plan => {
            weatherElement.innerHTML = plan.map(x => `<tr><td>${toDate(x.dt)}</td><td><img src='${ x.icon }'/></td><td>${x.min} °C</td></tr>`).join('');
        });
    };
    start(() => { weatherUpdate()}, 3600 * 1000);

    // fitness
    let fitnessElement = document.querySelector("#fitness table");
    let fitnessUpdate = () => {
        fetch('/fitone').then(x => x.json()).then(plan => {
            fitnessElement.innerHTML = plan.map(item => `<tr><td>${item.time}</td><td>${item.name}</td></tr>`).join('')
        });
    };
    start(() => { fitnessUpdate() }, 3600 * 1000);

    //keypresses
    let onKeyPress = (key) => {
        let body = document.body;
        if(key === 'select' && body.className === 'main') body.className = 'main time';
        else if(key === 'select' && body.className === 'main time') body.className = 'main';
        else if(key === 'red') { body.className = 'timer'; window.saveHueState();}
        else if(key === 'blue') body.className = 'main';
        else if(body.className === 'timer') onKeyPressTimer(key);
    };
    window.addEventListener('keydown', (e) => {
        const map = {ArrowLeft: 'left', ArrowRight: 'right', ArrowUp: 'up', ArrowDown: 'down', Enter:'select', Backspace:'exit', g:'green', b:'blue', y:'yellow', r:'red'};
        if(map[e.key]) {
            onKeyPress(map[e.key].toLowerCase());
        }
    });

    //hue
    window.hueConfig;
    window.hueState = [];
    fetch('/hue').then(x => x.json()).then(config => {
        window.hueConfig = config;
        hueConfig.link = `http://${config.ip}/api/${config.username}`;
    });
    window.saveHueState = () => {
        window.hueState = [];
        return Promise.all(
            hueConfig.lights.map(
                id => fetch(`${hueConfig.link}/lights/${id}`)
                    .then(x => x.json())
                    .then(x => window.hueState.push({
                        on: x.state.on, bri: x.state.bri, hue: x.state.hue, sat: x.state.sat, id: id
                    }))
            )
        );
    };
    window.loadHueState = () => {
        return Promise.all(
            hueState.map(
                state => fetch(`${hueConfig.link}/lights/${state.id}/state`, {method: 'PUT', body: JSON.stringify(state)})
                    .then(x => x.json())
                    .then(x => x)
            )
        );
    };
    window.setHue = (on, bri, sat, hue) => {
        return Promise.all(
            hueConfig.lights.map(id => fetch(`${hueConfig.link}/lights/${id}/state`, {method: 'PUT', body: JSON.stringify({on: on, bri: bri, sat: sat, hue: hue})})
                .then(x => x.json())
                .then(x => x)
            )
        );
    }

    //timer
    let timerRunning = false;
    let timerCount = 20;
    let originalTime = 60;
    let timerElement = document.querySelector("#timer .number");
    let timerProgress = document.querySelector("#timer .progress");
    let onKeyPressTimer = (key) => {
        if(key === 'select') setTimerRunning(!timerRunning);
        else if(key === 'play') setTimerRunning(true);
        else if(key === 'pause') setTimerRunning(false);
        else if(key === 'up' && !timerRunning) setTimerCount(originalTime);
        else if(key === 'left' && !timerRunning && timerCount>0) setTimerCount(timerCount-1);
        else if(key === 'right' && !timerRunning) setTimerCount(timerCount+1);
    };
    let setTimerRunning = (x) => {
        if(x) {
            originalTime = timerCount; 
        }
        timerRunning = x;
    };
    let setTimerCount = (x) => {
        if(x <= 0) {
            timerRunning = false;
            document.body.style.background = "white";
            window.setHue(true, 254, 254, 0);
            setTimeout(() => {
                document.body.style.background = "black";
                window.loadHueState();
            }, 1000);
        }
        if(x >= 0) {
            timerCount = x;
            timerElement.innerHTML = `${lz(Math.floor(timerCount/60))}:${lz(timerCount%60)}`;
            timerProgress.style.width = `${Math.floor(100*timerCount/originalTime)}%`;
            timerProgress.style.left = `${Math.floor(100-100*(timerCount===0?0:timerCount-1)/(originalTime-1))}%`;
        }
    }
    setInterval(() => {
        if(timerRunning) setTimerCount(timerCount-1);
    }, 1000);
    setTimerCount(timerCount);

    // websockets
    let reloadNeeded = false;
    const socket = io("http://localhost:3001");
    socket.on('connect', () => {
        if(reloadNeeded){
            location.reload();
        }else{
            show('timer');
            socket.emit('debug', `window w=${window.innerWidth} h=${window.innerHeight}`)
        }
    });
    socket.on('keypress', (msg) => {
        onKeyPress(msg);
    });
    socket.on('disconnect', () => {
        show('offline');
        reloadNeeded = true;
    });

});